# AREA 级联查询菜单组


联动下拉菜单

> 技术讨论 QQ 群 220127329

数据来源

>[中华人民共和国国家统计局>>行政区划代码](http://www.stats.gov.cn/tjsj/tjbz/xzqhdm/)
    
UI框架

>[Layui - 经典模块化前端框架](http://www.layui.com/)

## AREA 说明


> 简单介绍

```
该项目由 mybatisplus-spring-boot 搭建而成

    https://git.oschina.net/baomidou/mybatisplus-spring-boot

已集成组件：

1、mybatis-plus （mybatis 自动 crud 功能）
2、mybatis-spring-boot-starter （强大的ORM框架）
3、HikariCP（高性能数据源）
4、springloaded （热加载）
5、layui（经典模块化前端框架）
6、 等..

```


> 运行项目配置说明

```
1、修改 \area\src\main\resources\application.properties 数据库连接配置

2、导入数据库 \area\src\test\resources\area.sql

3、启动 \area\src\main\java\cn\wdcode\area\AreaApplication.java 里的main方法开始运行项目

4、访问：http://localhost:8080

```


更多补充中。。。。。


演示界面
=======

![效果演示](http://files.git.oschina.net/group1/M00/00/90/PaAvDFgdqqmAMs8HAACTcjvvmOk614.png?token=646abfcf8863438d0beb346af1edb676&ts=1478339221&attname=demo1.png)
![效果演示](http://files.git.oschina.net/group1/M00/00/90/PaAvDFgdqrCAFuEaAACWMgGce2M882.png?token=23dd72a66791422e28a48d682dde364e&ts=1478339221&attname=demo2.png)
![效果演示](http://files.git.oschina.net/group1/M00/00/90/PaAvDFgdqriAT9nDAAB9sSrs_XE489.png?token=32f096995775a71b6cac69a92beb99b1&ts=1478339221&attname=demo3.png)
![效果演示](http://files.git.oschina.net/group1/M00/00/90/PaAvDFgdqsCAD6JeAACRBXCrCC8659.png?token=19ed38de95fe4145b9bc9e0ee9161894&ts=1478339221&attname=demo4.png)
![效果演示](http://files.git.oschina.net/group1/M00/00/90/PaAvDFgdqseAV8vHAACK0tEQE5Y336.png?token=a97398ff22b286b2618803076dc44601&ts=1478339221&attname=demo5.png)




Features
=======

1、欢迎提出更好的意见，帮助完善 AREA

Copyright
====================
GPL V3 License