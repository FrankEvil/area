package cn.wdcode.area.controller;

import cn.wdcode.area.entity.Area;
import cn.wdcode.area.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Area 控制层
 */

//@Controller
@RestController
@RequestMapping("/Area")
public class AreaController {

    @Autowired
    private IAreaService AreaService;

    @GetMapping("/province/{areaId}")
    public List<Area> provinceDetail(@PathVariable Integer areaId) {
        if (areaId == null) {
            areaId = 100000;
        }
        Map<String, Object> columnMap = new HashMap<String, Object>();
        columnMap.put("area_parent_id", areaId);

        System.out.println("有参数");
        return AreaService.selectByMap(columnMap);
    }
    @GetMapping("/province")
    public List<Area> province() {

        System.out.println("无参数");
        return provinceDetail(null);
    }

//    @GetMapping("/city/{areaId}")
//    public Area city(@PathVariable Integer areaId) {
//
//        if (areaId == null) {
//            areaId = 0;
//        }
//        Map<String, Object> columnMap = new HashMap<String, Object>();
//        columnMap.put("area_parent_id", areaId);
//        return AreaService.selectByMap(columnMap);
//    }
//
//    @GetMapping("/district")
//    public Area district() {
//
//        return null;
//    }

}